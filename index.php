<?php

global $conn;
include('conn.php');

$sql = "SELECT employees.* , sections.name AS section_name FROM employees INNER JOIN sections ON employees.section_id = sections.id;";

$result = mysqli_query($conn, $sql);

$employees = mysqli_fetch_all($result, MYSQLI_ASSOC);

mysqli_free_result($result);


if (isset($_POST['delete'])) {

    $id = $_POST['id_delete'];

    $sql = "DELETE FROM employees WHERE id = $id;";

    if (mysqli_query($conn, $sql)) {
        header('location: index.php');
    } else {
        echo 'query error: ' . mysqli_error($conn);
    }

};


mysqli_close($conn);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<style>
    table, th, td {
        border: 1px solid black;
    }
</style>
<body>

<h2 style="background-color: black ; color: white ; font-weight: bold ; padding: .5rem 2rem ; border-radius: .4rem ;text-align: center ; width: 8rem ; margin: auto ; margin-bottom: 2rem ; margin-top: 2rem">
    employees</h2>

<div style="display: flex ; align-items: center ; justify-content: space-between">

    <a href="employees.php" style="background-color: black ; color: white ; font-weight: bold ; padding: .5rem 2rem ; border-radius: .4rem ;text-align: center ; width: 16rem ; margin: auto ; margin-bottom: 2rem ;text-decoration: none ; font-size: 1.5rem">All Employees</a>

    <h2 style="background-color: black ; color: white ; font-weight: bold ; padding: .5rem 2rem ; border-radius: .4rem ;text-align: center ; width: 16rem ; margin: auto ; margin-bottom: 2rem">
        Full Time Employees : <?php echo count($employees); ?></h2>
    <a href="sections.php" style="background-color: black ; color: white ; font-weight: bold ; padding: .5rem 2rem ; border-radius: .4rem ;text-align: center ; width: 16rem ; margin: auto ; margin-bottom: 2rem ; text-decoration: none ; font-size: 1.5rem">All Sections</a>

</div>

<div style="display: flex ; align-items: center ; justify-content: space-around">

    <a style="background-color: black ; color: white ; padding: .5rem 2rem ; font-weight: bold ; text-decoration: none; border-radius: .4rem"
       href="add.php">Add</a>
    <table style="width:50% ; margin: auto">
        <tr>
            <th>name</th>
            <th>phone</th>
            <th>email</th>
            <th>actions</th>
        </tr>

        <?php foreach ($employees as $employee) { ?>

            <tr>
                <td><?php echo $employee['name'] ?></td>
                <td><?php echo $employee['phone'] ?></td>
                <td><?php echo $employee['email'] ?></td>
                <td style="display: flex ; justify-content: space-around">
                    <button><a href="edit.php?id=<?php echo $employee['id'] ?>">Edit</a></button>
                    <button><a href="information.php?id=<?php echo $employee['id'] ?>">Information</a></button>
                    <form method="post">
                        <input type="hidden" name="id_delete" value="<?php echo $employee['id'] ?>">
                        <input name="delete" value="Delete" type="submit">
                    </form>
                </td>
            </tr>
        <?php } ?>

    </table>

</div>

</body>
</html>