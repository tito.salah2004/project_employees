<?php

global $conn;
include ('conn.php');

$sql = "SELECT employees.id, 
               COALESCE(employees.name, 'NULL') AS name,
               COALESCE(employees.phone, 'NULL') AS phone,
               COALESCE(employees.email , 'NULL') AS email, 
               COALESCE(sections.name, 'NULL') AS section_name
        FROM employees 
        RIGHT JOIN sections 
        ON employees.section_id = sections.id;";

$result = mysqli_query($conn, $sql);

$employees = mysqli_fetch_all($result, MYSQLI_ASSOC);

mysqli_free_result($result);

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<style>
    table, th, td {
        border: 1px solid black;
    }
</style>
<body>

<h2 style="background-color: black ; color: white ; font-weight: bold ; padding: .5rem 2rem ; border-radius: .4rem ;text-align: center ; width: 8rem ; margin: auto ; margin-bottom: 2rem ; margin-top: 2rem">
    employees</h2>

<div style="display: flex ; align-items: center ; justify-content: space-between">

    <h2 style="background-color: black ; color: white ; font-weight: bold ; padding: .5rem 2rem ; border-radius: .4rem ;text-align: center ; width: 16rem ; margin: auto ; margin-bottom: 2rem">
        All sections : <?php echo count($employees); ?></h2>

</div>

<div style="display: flex ; align-items: center ; justify-content: space-around">

    <table style="width:50% ; margin: auto">
        <tr>
            <th>id</th>
            <th>name</th>
            <th>phone</th>
            <th>email</th>
            <th>section_name</th>
        </tr>

        <?php foreach ($employees as $employee) { ?>

            <tr style="text-align: center">
                <td><?php echo $employee['id'] ?></td>
                <td><?php echo $employee['name'] ?></td>
                <td><?php echo $employee['phone'] ?></td>
                <td><?php echo $employee['email'] ?></td>
                <td><?php echo $employee['section_name'] ?></td>
            </tr>
        <?php } ?>

    </table>

</div>

</body>
</html>