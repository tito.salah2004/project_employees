<?php

global $conn;
include('conn.php');

$sql_section = "SELECT * FROM sections";

$result = mysqli_query($conn, $sql_section);

$sections = mysqli_fetch_all($result, MYSQLI_ASSOC);

mysqli_free_result($result);


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $section_id = $_POST['section_id'];

    $sql = "INSERT INTO employees(name , phone , email, section_id) VALUES ('$name' , '$phone' , '$email', '$section_id')";

    if (mysqli_query($conn, $sql)) {
        header('location: index.php');
    } else {
        echo 'ERROR connection';
    };

};

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit</title>
</head>
<body>

<form action="" method="POST">

    <input type="text" name="name">

    <input type="number" name="phone">

    <input type="email" name="email">

    <select name="section_id">
        <?php foreach ($sections as $section) { ?>
            <option value="<?php echo $section['id'] ?>"><?php echo $section['name'] ?></option>
        <?php } ?>
    </select>

    <button class="submit">Submit</button>

</form>

</body>
</html>