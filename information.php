<?php

global $conn;
include('conn.php');

if (isset($_GET['id'])) {

    $id = mysqli_real_escape_string($conn , $_GET['id']);

    $sql = "SELECT 
        employees.*, 
        sections.name AS section_name
        FROM employees
        INNER JOIN sections 
        ON employees.section_id = sections.id
        WHERE employees.id = $id";

    $result = mysqli_query($conn, $sql);

    $employee = mysqli_fetch_assoc($result);

    mysqli_free_result($result);

};

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<style>
    table, th, td {
        border: 1px solid black;
    }
</style>
<body>

<h2 style="background-color: black ; color: white ; font-weight: bold ; padding: .5rem 2rem ; border-radius: .4rem ;text-align: center ; width: 8rem ; margin: auto ; margin-bottom: 2rem ; margin-top: 2rem">
    employee</h2>

<h2 style="background-color: black ; color: white ; font-weight: bold ; padding: .5rem 2rem ; border-radius: .4rem ;text-align: center ; width: 10rem ; margin: auto ; margin-bottom: 2rem"><?php echo $employee['name'] ?> Information</h2>

<div style="display: flex ; align-items: center ; justify-content: space-around">

    <table style="width:50% ; margin: auto">
        <tr>
            <th>id</th>
            <th>names</th>
            <th>phones</th>
            <th>emails</th>
            <th>section_name</th>
        </tr>

            <tr style="text-align: center">
                <td><?php echo $employee['id'] ?></td>
                <td><?php echo $employee['name'] ?></td>
                <td><?php echo $employee['phone'] ?></td>
                <td><?php echo $employee['email'] ?></td>
                <td><?php echo $employee['section_name'] ?></td>
            </tr>

    </table>

</div>

</body>
</html>