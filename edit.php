<?php

global $conn;
include('conn.php');

if (isset($_GET['id'])) {

    $id = $_GET['id'];
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $section_id = $_POST['section_id'];


    $sql = "SELECT 
        employees.*, 
        sections.name AS section_name
        FROM employees
        INNER JOIN sections 
        ON employees.section_id = sections.id
        WHERE employees.id = $id";

    $result = mysqli_query($conn, $sql);

    $employees = mysqli_fetch_assoc($result);

    mysqli_free_result($result);

    $sql_section = "SELECT * FROM sections";

    $result = mysqli_query($conn, $sql_section);

    $sections = mysqli_fetch_all($result, MYSQLI_ASSOC);

    mysqli_free_result($result);

};

if (isset($_POST['submit'])) {

    $id = $_GET['id'];
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $section_id = $_POST['section_id'];

    $sql = "UPDATE employees
INNER JOIN sections ON sections.id = employees.section_id
SET
    employees.name = '$name',
    employees.email = '$email',
    employees.phone = '$phone',
    employees.section_id = '$section_id'
WHERE employees.id = '$id';";


    if (mysqli_query($conn, $sql)) {
        header('location: index.php');
    } else {
        echo 'query error: ' . mysqli_error($conn);
    }

    mysqli_close($conn);

};

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit</title>
</head>
<body>

<form action="<?php ($_SERVER['PHP_SELF']) ?>" method="post">

    <input type="text" name="name" value="<?php echo $employees['name'] ?>">

    <input type="number" name="phone" value="<?php echo $employees['phone'] ?>">

    <input type="email" name="email" value="<?php echo $employees['email'] ?>">

    <select name="section_id">
        <?php foreach ($sections as $section) { ?>
            <option value="<?php echo $section['id'] ?>"><?php echo $section['name'] ?></option>
        <?php } ?>
    </select>

    <button type="submit" name="submit">Submit</button>

</form>

</body>
</html>